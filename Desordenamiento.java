import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Desordenamiento {
    public static void main(String[] args) {
        List <Integer>lista = new ArrayList<Integer>();

        for (int i = 0; i < 50; i++){
            lista.add(i);
        }

        List desordenada = desordenar(lista);
        System.out.println("Lista desordenada: " + desordenada);
    }

    public static List desordenar(List lista){
        List <Integer> listaDesordenada = new ArrayList<>();
        while(!lista.isEmpty()){
            int aleatorio = new Random().nextInt(lista.size());
            Integer tmp = (Integer) lista.get(aleatorio);
            listaDesordenada.add(tmp);
            lista.remove(aleatorio);
        }
        return listaDesordenada;
    }
}