import java.util.Random;
import java.util.Scanner;

/**
* Clase que adivina un numero
* @author Zeuxis Villegas <villegasmorenodaniel@gmail.com>
*/
public class AdivinaNumero {
    public static void main(String[] args) {
        byte intentos = 5;
        byte rangomaximo = 10;
        Scanner input = new Scanner(System.in);
        byte guess = 0;

        //Recupera el rango de valores
        //Si no se proporciono un rango se debe usar del 1 al 10
        //El numero de intentos default es 5
        if (args.length == 1){
            rangomaximo = Byte.parseByte(args[0]);
        }else if(args.length == 2){
            rangomaximo = Byte.parseByte(args[0]);
            intentos = Byte.parseByte(args[1]);
        }
        byte intentosCopia = intentos;
        boolean volverAJugar = true;
        while(volverAJugar) {
            System.out.println();
            System.out.printf("Adivina un numero entre 0 y %d %n", rangomaximo);
            int aleatorio = new Random().nextInt(rangomaximo);
            System.out.println("Número generado aleatoriamente (para probar la funcionalidad adivina a la primera): " + aleatorio);

        //Ciclo de comparacion
            while (intentos > 0) {
                System.out.println();
                System.out.println("Numero de intento: " + intentos);
                System.out.println("Que numero?");
                guess = input.nextByte();
                System.out.println("guess: " + guess);
                if (guess != aleatorio) {
                    if (guess > aleatorio) {
                        System.out.println("Perdiste, el numero a adivinar es mas pequeño, intenta de nuevo");
                    } else {
                        System.out.println("Perdiste, el numero a adivinar es mas grande, intenta de nuevo");
                    }
                    intentos--;
                } else {
                    System.out.println("Ganaste!");
                    if (intentos == intentosCopia) {
                        System.out.println("AWESOME! adivinaste a la primera");
                    }
                    intentos = 0;
                }
            }
            System.out.println();
            System.out.println("El numero era: " + aleatorio);
            System.out.println();
            System.out.println("Deseas volver a jugar? (true/false)");
            if (volverAJugar = input.nextBoolean()){
                intentos = intentosCopia;
            }
        }

        //{
        //Pide el valor

        //Compara contra el valor proporcionado, si es incorrecto decrementa los intentos y muestra
        //Si es correcto muestra el valor correcto
        //}

        //Extra: Imprimir Awesome si adivino a la primera
        //Extra: Preguntar si desea jugar de nuevo
    }
}