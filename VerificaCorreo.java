public class VerificaCorreo{
	public static void main(String[] args) {
		if (args.length == 1) {
			String correoAValidadar = args[0];
			// char caracteresEspeciales[] = null; 
			// byte fase = 0;
			if (correoAValidadar.length() < 6) {
				System.out.println("Invalido: El correo debe ser mayor a 5 caracteres");
			}else{
				int usuarioIndex = correoAValidadar.indexOf("@");
				if (usuarioIndex <= 0) {
					System.out.println("Invalido: El correo no contiene Arroba o nombre de usuario");
				}else {
					String organizacion = correoAValidadar.substring(usuarioIndex + 1);
					System.out.println("organizacion: " + organizacion);
					if (organizacion.indexOf("@") > -1) {
						System.out.println("Invalido: El correo contiene más de un Arroba");
					}else{
						int dominioIndex = organizacion.indexOf(".");
						if (dominioIndex <= 0) {
							System.out.println("Invalido: el correo no contiene . o nombre de organización");
						}else{
							String dominio = organizacion.substring(dominioIndex + 1);
							switch(dominio){
								case "com":
									System.out.println("Valido");
									break;
								case "net":
									System.out.println("Valido");
								break;
								case "org":
									System.out.println("Valido");
								break;
								case "mx":
									System.out.println("Valido");
								break;
								default:
									System.out.println("Invalido: El dominio no cumple con los especificados");
								break;
							}
						}
					}
				}
			}

		}else{
			System.out.println("Favor de ingresar por parametro el correo a validar al ejecutar el programa");
		}
	}
}